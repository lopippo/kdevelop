# Translation of kdevopenwith to Norwegian Bokmål
#
# Bjørn Steensrud <bjornst@skogkatt.homelinux.org>, 2010.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-11-05 12:28+0000\n"
"PO-Revision-Date: 2010-12-30 22:01+0100\n"
"Last-Translator: Bjørn Steensrud <bjornst@skogkatt.homelinux.org>\n"
"Language-Team: Norwegian Bokmål <l10n-no@lister.huftis.org>\n"
"Language: nb\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.1\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Environment: kde\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#: openwithplugin.cpp:139
#, kde-format
msgctxt "@item:menu"
msgid "Other..."
msgstr ""

#: openwithplugin.cpp:150
#, kde-format
msgctxt "@title:menu"
msgid "Open With"
msgstr ""

#: openwithplugin.cpp:155
#, kde-format
msgctxt "@title:menu"
msgid "Embedded Editors"
msgstr ""

#: openwithplugin.cpp:159
#, kde-format
msgctxt "@title:menu"
msgid "External Applications"
msgstr ""

#: openwithplugin.cpp:166
#, kde-format
msgctxt "@action:inmenu"
msgid "Open"
msgstr ""

#: openwithplugin.cpp:186
#, kde-format
msgctxt "@item:inmenu"
msgid "Default Editor"
msgstr ""

#: openwithplugin.cpp:272
#, kde-format
msgctxt "%1: mime type name, %2: app/part name"
msgid "Do you want to open all '%1' files by default with %2?"
msgstr ""

#: openwithplugin.cpp:274
#, kde-format
msgctxt "@title:window"
msgid "Set as Default?"
msgstr ""

#: openwithplugin.cpp:275
#, kde-format
msgctxt "@action:button"
msgid "Set as Default"
msgstr ""

#: openwithplugin.cpp:276
#, kde-format
msgctxt "@action:button"
msgid "Do Not Set"
msgstr ""
